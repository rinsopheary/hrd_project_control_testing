package com.kshrd.hrdprojectcontrolapi.services.project;

import com.kshrd.hrdprojectcontrolapi.models.Project;
import com.kshrd.hrdprojectcontrolapi.repositories.project.ProjectRepository;
import com.kshrd.hrdprojectcontrolapi.services.project.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectServiceImp implements ProjectService {

    private ProjectRepository projectRepository;

    @Autowired
    public ProjectServiceImp(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }


    @Override
    public List<Project> selectAll() {
        return projectRepository.selectAll();
    }
}
