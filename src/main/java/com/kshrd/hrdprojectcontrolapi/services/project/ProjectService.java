package com.kshrd.hrdprojectcontrolapi.services.project;

import com.kshrd.hrdprojectcontrolapi.models.Project;

import java.util.List;

public interface ProjectService {

    List<Project> selectAll();
}
