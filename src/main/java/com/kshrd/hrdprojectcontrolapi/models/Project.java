package com.kshrd.hrdprojectcontrolapi.models;

public class Project {

    private Integer projectId;

    private String title;

    private String objective;

    private String feature;

    public Project() {
    }

    public Project(Integer projectId, String title, String objective, String feature) {
        this.projectId = projectId;
        this.title = title;
        this.objective = objective;
        this.feature = feature;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    @Override
    public String toString() {
        return "project{" +
                "projectId=" + projectId +
                ", title='" + title + '\'' +
                ", objective='" + objective + '\'' +
                ", feature='" + feature + '\'' +
                '}';
    }
}
