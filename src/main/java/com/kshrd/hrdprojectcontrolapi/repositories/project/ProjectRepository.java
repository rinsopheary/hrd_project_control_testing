package com.kshrd.hrdprojectcontrolapi.repositories.project;

import com.kshrd.hrdprojectcontrolapi.models.Project;
import com.kshrd.hrdprojectcontrolapi.repositories.project.ProjectProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository {

    @SelectProvider(value = ProjectProvider.class, method = "selectAll")
    @Results({
            @Result(column = "project_id", property = "projectId"),
            @Result(column = "project_title", property = "title"),
            @Result(column = "project_objective", property = "objective"),
            @Result(column = "project_feature", property = "feature")
    })
    List<Project> selectAll();

}
