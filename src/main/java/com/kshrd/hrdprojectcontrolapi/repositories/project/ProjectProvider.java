package com.kshrd.hrdprojectcontrolapi.repositories.project;

import org.apache.ibatis.jdbc.SQL;

public class ProjectProvider {

    public String selectAll() {
        return new SQL() {{
            SELECT("*");
            FROM("project");
        }}.toString();
    }

}
