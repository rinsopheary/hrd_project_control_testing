package com.kshrd.hrdprojectcontrolapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HrdProjectControlApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(HrdProjectControlApiApplication.class, args);
    }

}
