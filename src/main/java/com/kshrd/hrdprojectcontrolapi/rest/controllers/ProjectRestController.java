package com.kshrd.hrdprojectcontrolapi.rest.controllers;

import com.kshrd.hrdprojectcontrolapi.models.Project;
import com.kshrd.hrdprojectcontrolapi.rest.response.BaseApiResponse;
import com.kshrd.hrdprojectcontrolapi.services.project.ProjectServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/projects")
public class ProjectRestController {

    private ProjectServiceImp projectServiceImp;

    @Autowired
    public ProjectRestController(ProjectServiceImp projectServiceImp) {
        this.projectServiceImp = projectServiceImp;
    }

    @GetMapping
    public ResponseEntity<BaseApiResponse<List<Project>>> selectAll() {

        BaseApiResponse<List<Project>> apiResponse = new BaseApiResponse<>();

        List<Project> projects = projectServiceImp.selectAll();

        apiResponse.setMessage("All project have been found");
        apiResponse.setData(projects);
        apiResponse.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(apiResponse);
    }
}
