package com.kshrd.hrdprojectcontrolapi.rest.request;

public class ProjectApiRequest {

    private String title;

    private String objective;

    private String feature;

    public ProjectApiRequest() {
    }

    public ProjectApiRequest(String title, String objective, String feature) {
        this.title = title;
        this.objective = objective;
        this.feature = feature;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    @Override
    public String toString() {
        return "ProjectApiRequest{" +
                "title='" + title + '\'' +
                ", objective='" + objective + '\'' +
                ", feature='" + feature + '\'' +
                '}';
    }
}
