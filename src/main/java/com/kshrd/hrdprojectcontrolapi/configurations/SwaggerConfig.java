package com.kshrd.hrdprojectcontrolapi.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("HRD Project Control API")
                .description("Korea Software HRD Center(KSHRD) \uD83D\uDC95")
                .license("License of API")
                .licenseUrl("Email")
                .termsOfServiceUrl("https://spring.io/")
                .version("8th Basic Course")
                .contact(new Contact("By HRD Project Control team", "https://spring.io/", "hpc@gmail.com"))
                .build();
    }

    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
                .build().apiInfo(apiInfo());
    }
}